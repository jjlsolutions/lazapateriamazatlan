-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2022 a las 07:35:13
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lazapateriamazatlan`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `borrarOpcionPerfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrarOpcionPerfil` (IN `ban` INT, IN `cvePerfil_i` INT)  BEGIN
	
	-- 1. Elimina todas las opciones dependiendo el Perfil
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM de_perfilopcion WHERE cveperfil_perfilopcion = cvePerfil_i;
			
		
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarCliente`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarCliente` (IN `ban` INT, IN `cveCliente_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el cliente de la BD
	-- 2. Cambia el estatus del cliente a 0
	-- 3. Cambia el estatus del cliente a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_clientes WHERE cve_cliente = cveCliente_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_clientes 
					SET estatus_cliente = 0,
							cveusuariomod_cliente = cveUsuarioAccion_i,
							fechamod_cliente = NOW()
					WHERE cve_cliente = cveCliente_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_clientes 
					SET estatus_cliente = 1,
							cveusuariomod_cliente = cveUsuarioAccion_i,
							fechamod_cliente = NOW()
					WHERE cve_cliente = cveCliente_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarListaventa`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarListaventa` (IN `ban` INT, IN `cve_venta_i` INT, IN `motivo_i` VARCHAR(100), IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina la producto de la BD
	-- 2. Cambia el estatus de la producto a 0
	-- 3. Cambia el estatus de la producto a 1
	
	CASE
		WHEN ban = 1 THEN
		
			UPDATE ma_ventas mv
					SET mv.estatus_venta = 1,
					mv.motivo_venta = '',
							mv.cveusuariomod_venta = cveUsuarioAccion_i,
							mv.fechamod_venta = NOW()
					WHERE mv.cve_venta = cve_venta_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ma_ventas mv
					SET mv.estatus_venta = 2,
					mv.motivo_venta = motivo_i,
							mv.cveusuariomod_venta = cveUsuarioAccion_i,
							mv.fechamod_venta = NOW()
					WHERE mv.cve_venta = cve_venta_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ma_ventas mv
					SET mv.estatus_venta = 3,
							mv.cveusuariomod_venta = cveUsuarioAccion_i,
							mv.fechamod_venta = NOW()
					WHERE mv.cve_venta = cve_venta_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarPerfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarPerfil` (IN `ban` INT, IN `cvePerfil_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el perfil de la BD
	-- 2. Cambia el estatus del perfil a 0
	-- 3. Cambia el estatus del perfil a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_perfil WHERE cve_perfil = cvePerfil_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_perfil 
					SET estatus_perfil = 0,
							cveusuariomod_perfil = cveUsuarioAccion_i,
							fechamod_perfil = NOW()
					WHERE cve_perfil = cvePerfil_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_perfil 
					SET estatus_perfil = 1,
							cveusuariomod_perfil = cveUsuarioAccion_i,
							fechamod_perfil = NOW()
					WHERE cve_perfil = cvePerfil_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarProducto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarProducto` (IN `ban` INT, IN `cve_producto_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina la producto de la BD
	-- 2. Cambia el estatus de la producto a 0
	-- 3. Cambia el estatus de la producto a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_productos WHERE cve_producto = cve_producto_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_productos 
					SET estatus_producto = 0,
							cveusuariomod_producto = cveUsuarioAccion_i,
							fechamod_producto = NOW()
					WHERE cve_producto = cve_producto_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_productos 
					SET estatus_producto = 1,
							cveusuariomod_producto = cveUsuarioAccion_i,
							fechamod_producto = NOW()
					WHERE cve_producto = cve_producto_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarPuesto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarPuesto` (IN `ban` INT, IN `cvePuesto_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el puesto de la BD
	-- 2. Cambia el estatus del puesto a 0
	-- 3. Cambia el estatus del puesto a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_puestos WHERE cve_puesto = cvePuesto_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_puestos 
					SET estatus_puesto = 0,
							cveusuariomod_puesto = cveUsuarioAccion_i,
							fechamod_puesto = NOW()
					WHERE cve_puesto = cvePuesto_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_puestos 
					SET estatus_puesto = 1,
							cveusuariomod_puesto = cveUsuarioAccion_i,
							fechamod_puesto = NOW()
					WHERE cve_puesto = cvePuesto_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarSabor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarSabor` (IN `ban` INT, IN `cve_sabor_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el sabor de la BD
	-- 2. Cambia el estatus del sabor a 0
	-- 3. Cambia el estatus del sabor a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_sabores WHERE cve_sabor = cve_sabor_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_sabores 
					SET estatus_sabor = 0,
							cveusuariomod_sabor = cveUsuarioAccion_i,
							fechamod_sabor = NOW()
					WHERE cve_sabor = cve_sabor_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_sabores 
					SET estatus_sabor = 1,
							cveusuariomod_sabor = cveUsuarioAccion_i,
							fechamod_sabor = NOW()
					WHERE cve_sabor = cve_sabor_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarSucursal`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarSucursal` (IN `ban` INT, IN `cveSucursal_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina la sucursal de la BD
	-- 2. Cambia el estatus de la sucursal a 0
	-- 3. Cambia el estatus de la sucursal a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_sucursales WHERE cve_sucursal = cveSucursal_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_sucursales 
					SET estatus_sucursal = 0,
							cveusuariomod_sucursal = cveUsuarioAccion_i,
							fechamod_sucursal = NOW()
					WHERE cve_sucursal = cveSucursal_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_sucursales 
					SET estatus_sucursal = 1,
							cveusuariomod_sucursal = cveUsuarioAccion_i,
							fechamod_sucursal = NOW()
					WHERE cve_sucursal = cveSucursal_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `eliminarUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminarUsuario` (IN `ban` INT, IN `cveUsuario_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Elimina el usuario de la BD
	-- 2. Cambia el estatus del usuario a 0
	-- 3. Cambia el estatus del usuario a 1
	
	CASE
		WHEN ban = 1 THEN
		
			DELETE FROM ca_usuario WHERE cve_usuario = cveUsuario_i;
			
		WHEN ban = 2 THEN
		
			UPDATE ca_usuario 
					SET estatus_usuario = 0,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
					
		WHEN ban = 3 THEN
		
			UPDATE ca_usuario 
					SET estatus_usuario = 1,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `generarFolio`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `generarFolio` (IN `ban` INT, IN `folo_venta_i` VARCHAR(100), IN `total_venta_i` DOUBLE, IN `tipo_venta_i` INT, IN `tipoprecio_venta_i` INT, IN `cvecliente_venta_i` INT, IN `cobroenvio_venta_i` INT, IN `cve_sucursal_i` INT, IN `cveUsuarioAccion_i` INT)  begin
	-- 1. Guarda un bebida y guarda cambios
	
	declare v_folio varchar(100);
	declare v_folio_venta varchar(100);
	declare v_count int;
	declare v_ultmofolio int;
	declare v_cve_venta int;
	declare v_countTotal int;
	
	CASE
		WHEN ban = 1 THEN
		
			IF folo_venta_i = '' then
			
					set v_folio = SUBSTR(DATE_FORMAT(now(), '%d%m%Y'), 1, 8);
	
	                select count(*) into  v_count FROM ma_ventas mv 
	                WHERE folio_venta LIKE concat(v_folio, '%');
	               
	               if v_count > 0 then
	
						select 
							SUBSTR(folio_venta, 9, 7) into v_ultmofolio
						from 
							ma_ventas 
						where
							folio_venta like concat(v_folio , '%')
						order by
							cve_venta desc LIMIT 1;
		                 set v_folio_venta := CONCAT (v_folio, LPAD(v_ultmofolio+1, 3, '0'));
	               ELSE
	                   set v_folio_venta := CONCAT (v_folio, LPAD(1, 3, '0'));
	               END IF;
					
				
					INSERT INTO ma_ventas (
							folio_venta ,
							total_venta,
							tipo_venta,
							tipoprecio_venta,
							cvecliente_venta,
							cobroenvio_venta,
							cvesucursal_venta,
							cveusuarioadd_venta
						) VALUES (
							v_folio_venta,
							total_venta_i,
							tipo_venta_i,
							tipoprecio_venta_i,
							cvecliente_venta_i,
							cobroenvio_venta_i,
							cve_sucursal_i,
							cveUsuarioAccion_i
						);
					
					SELECT last_insert_id() as cve_venta;
				
			else
				delete from ma_ventas where folio_venta = folo_venta_i;
			END IF;
		
	END CASE;
END$$

DROP PROCEDURE IF EXISTS `guardarCliente`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarCliente` (IN `ban` INT, IN `cveCliente_i` INT, IN `nombreCliente_i` VARCHAR(100), IN `direccionCliente_i` VARCHAR(120), IN `comentarioCliente_i` VARCHAR(75), IN `celularCliente_i` VARCHAR(150), IN `cve_sucursal_i` INT, IN `cveUsuarioAccion_i` INT)  begin
	
	-- 1. Guarda una cliente y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cveCliente_i < 1 THEN
			
				INSERT INTO ca_clientes  
					(
					
					nombre_cliente,
					celular_cliente,
					direccion_cliente,
					comentario_cliente,
					estatus_cliente,
					cveusuarioadd_cliente,
					cvesucursal_cliente,
					fechaadd_cliente
					) VALUES (
										nombreCliente_i,
										celularCliente_i,
										direccionCliente_i,
										comentarioCliente_i,
										1,
										cveUsuarioAccion_i,
										cve_sucursal_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_clientes 
				SET 
						nombre_cliente = nombreCliente_i,
						celular_cliente = celularCliente_i,
						direccion_cliente = direccionCliente_i,
						comentario_cliente = comentarioCliente_i,
						cveusuariomod_cliente = cveUsuarioAccion_i,
						fechamod_cliente = NOW()
				WHERE cve_cliente = cveCliente_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarDetalleVenta`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarDetalleVenta` (IN `ban` INT, IN `cveventa_venta_i` INT, IN `cvesp_venta_i` INT, IN `cantidad_venta_i` INT, IN `precio_venta_i` DOUBLE)  BEGIN
	
	-- 1. Guarda una producto y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
			
				INSERT INTO de_venta  
					(
					cveventa_venta,
					cvesp_venta,
					cantidad_venta,
					precio_venta
					) VALUES (
	                    cveventa_venta_i,
						cvesp_venta_i,
						cantidad_venta_i,
						precio_venta_i
					 );
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarPerfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarPerfil` (IN `ban` INT, IN `cvePerfil_i` INT, IN `nombrePerfil_i` VARCHAR(120), IN `descipcionPerfil_i` VARCHAR(250), IN `cve_sucursal_i` INT, IN `cveOpcion_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda un usuario y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cvePerfil_i < 1 THEN
			
				INSERT INTO ca_perfil  
					(nombre_perfil,
					 descripcion_perfil,
					 estatus_perfil,
					 cveusuarioalta_perfil,
					 cvesucursal_perfil,
					 fechaalta_perfil
					) VALUES (nombrePerfil_i,
										descipcionPerfil_i,
										1,
										cveUsuarioAccion_i,
										cve_sucursal_i,
										NOW()
									 );
									 
				SELECT MAX(cve_perfil) AS cve FROM ca_perfil;
				
			ELSE
			
				UPDATE ca_perfil 
					SET nombre_perfil = nombrePerfil_i,
							descripcion_perfil = descipcionPerfil_i,
							cveusuariomod_perfil = cveUsuarioAccion_i,
							fechamod_perfil = NOW()
					WHERE cve_perfil = cvePerfil_i;
					
					SELECT cve_perfil FROM ca_perfil WHERE cve_perfil = cvePerfil_i;
				
			END IF;
			
		WHEN ban = 2 THEN
		
			INSERT INTO de_perfilopcion (cveperfil_perfilopcion,cveopcion_perfilopcion) VALUES (cvePerfil_i,cveOpcion_i);
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarProducto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarProducto` (IN `ban` INT, IN `cve_producto_i` INT, IN `nombreProducto_i` VARCHAR(100), IN `preciomayoreo_producto_i` VARCHAR(150), IN `preciomenudeo_producto_i` VARCHAR(120), IN `cve_sucursal_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda una producto y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cve_producto_i < 1 THEN
			
				INSERT INTO ca_productos  
					(
					nombre_producto,
					preciomayoreo_producto,
					preciomenudeo_producto,
				    cveusuarioadd_producto,
				    cvesucursal_producto,
					fechaadd_productos
					) VALUES (
					                    nombreProducto_i,
										preciomayoreo_producto_i,
										preciomenudeo_producto_i,
										cveUsuarioAccion_i,
										cve_sucursal_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_productos 
				SET 
				        nombre_producto = nombreProducto_i,
						preciomayoreo_producto = preciomayoreo_producto_i,
						preciomenudeo_producto = preciomenudeo_producto_i,
						cveusuariomod_producto = cveUsuarioAccion_i,
						fechamod_producto = NOW()
				WHERE cve_producto = cve_producto_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarPuesto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarPuesto` (IN `ban` INT, IN `cvePuesto_i` INT, IN `nombrePuesto_i` VARCHAR(100), IN `cve_sucursal_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda un Puesto y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cvePuesto_i < 1 THEN
			
				INSERT INTO ca_puestos  
					(
					nombre_puesto,
					estatus_puesto,
					cveusuarioalta_puesto,
					cvesucursal_puesto,
					fechaalta_puesto
					) VALUES (nombrePuesto_i,
										1,
										cveUsuarioAccion_i,
										cve_sucursal_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_puestos 
				SET nombre_puesto = nombrePuesto_i,
						cveusuariomod_puesto = cveUsuarioAccion_i,
						fechamod_puesto = NOW()
				WHERE cve_puesto = cvePuesto_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarSabor`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarSabor` (IN `ban` INT, IN `cve_sabor_i` INT, IN `nombre_sabor_i` VARCHAR(100), IN `cveproducto_sabor_i` VARCHAR(150), IN `clave_sabor_i` VARCHAR(120), IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda una sabor y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cve_sabor_i < 1 THEN
			
				INSERT INTO ca_sabores  
					(
					nombre_sabor,
					cveproducto_sabor,
					clave_sabor,
					cveusuarioadd_sabor,
					fechaadd_sabor
					) VALUES (
										nombre_sabor_i,
										cveproducto_sabor_i,
										clave_sabor_i,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_sabores 
				SET 	nombre_sabor = nombre_sabor_i,
						cveproducto_sabor = cveproducto_sabor_i,
						clave_sabor = clave_sabor_i,
						cveusuariomod_sabor = cveUsuarioAccion_i,
						fechamod_sabor = NOW()
				WHERE cve_sabor = cve_sabor_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarSucursal`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarSucursal` (IN `ban` INT, IN `cve_sucursal_i` INT, IN `nombre_sucursal_i` VARCHAR(100), IN `calle_sucursal_i` VARCHAR(150), IN `colonia_sucursal_i` VARCHAR(120), IN `telefono_sucursal_i` VARCHAR(75), IN `representante_sucursal_i` VARCHAR(120), IN `tipo_sucursal_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN
	
	-- 1. Guarda una sucursal y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cve_sucursal_i < 1 THEN
			
				INSERT INTO ca_sucursales  
					(
					tipo_sucursal,
					nombre_sucursal,
					calle_sucursal,
					colonia_sucursal,
					telefono_sucursal,
					representante_sucursal,
					estatus_sucursal,
					cveusuarioalta_sucursal,
					fechaalta_sucursal
					) VALUES (tipo_sucursal_i,
										nombre_sucursal_i,
										calle_sucursal_i,
										colonia_sucursal_i,
										telefono_sucursal_i,
										representante_sucursal_i,
										1,
										cveUsuarioAccion_i,
										NOW()
									 );
				
			ELSE
			
				UPDATE ca_sucursales 
				SET tipo_sucursal = tipo_sucursal_i,
						nombre_sucursal = nombre_sucursal_i,
						calle_sucursal = calle_sucursal_i,
						colonia_sucursal = colonia_sucursal_i,
						telefono_sucursal = telefono_sucursal_i,
						representante_sucursal = representante_sucursal_i,
						cveusuariomod_sucursal = cveUsuarioAccion_i,
						fechamod_sucursal = NOW()
				WHERE cve_sucursal = cve_sucursal_i;
				
			END IF;
		
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `guardarUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardarUsuario` (IN `ban` INT, IN `cveUsuario_i` INT, IN `nombreUsuario_i` VARCHAR(75), IN `apellidopUsuario_i` VARCHAR(75), IN `apellidomUsuario_i` VARCHAR(75), IN `loginUsuario_i` VARCHAR(35), IN `passwordUsuario_i` VARCHAR(75), IN `perfilUsuario_i` INT, IN `sucursal_i` INT, IN `puesto_i` INT, IN `cveUsuarioAccion_i` INT)  BEGIN

	-- 1. Guarda un usuario y guarda cambios
	
	CASE
		WHEN ban = 1 THEN
		
			IF cveUsuario_i < 1 THEN
			 
				INSERT INTO ca_usuario  
					(
					 cveperfil_usuario,
					 cvesucursal_usuario,
					 cvepuesto_usuario,
					 login_usuario,
					 password_usuario,
					 nombre_usuario,
					 apellidop_usuario,
					 apellidom_usuario,
					 estatus_usuario,
					 cveusuarioalta_usuario,
					 fechaalta_usuario
					) VALUES (perfilUsuario_i,
										sucursal_i,
										puesto_i,
										loginUsuario_i,
										passwordUsuario_i,
										nombreUsuario_i,
										apellidopUsuario_i,
										apellidomUsuario_i,
										'1',
										cveUsuarioAccion_i,
										NOW());
			
			ELSE
			
				IF passwordUsuario_i = "" THEN
				
					UPDATE ca_usuario 
					SET cveperfil_usuario = perfilUsuario_i,
							cvesucursal_usuario = sucursal_i,
							cvepuesto_usuario = puesto_i,
							login_usuario = loginUsuario_i,
							nombre_usuario = nombreUsuario_i,
							apellidop_usuario = apellidopUsuario_i,
							apellidom_usuario = apellidomUsuario_i,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
					
				ELSE
				
					UPDATE ca_usuario 
					SET cveperfil_usuario = perfilUsuario_i,
							cvesucursal_usuario = sucursal_i,
							cvepuesto_usuario = puesto_i,
							login_usuario = loginUsuario_i,
							password_usuario = passwordUsuario_i,
							nombre_usuario = nombreUsuario_i,
							apellidop_usuario = apellidopUsuario_i,
							apellidom_usuario = apellidomUsuario_i,
							cveusuariomod_usuario = cveUsuarioAccion_i,
							fechamod_usuario = NOW()
					WHERE cve_usuario = cveUsuario_i;
				
				END IF;
			
			
			END IF;
			
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `loginUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `loginUsuario` (IN `usuario` VARCHAR(75), IN `pass` VARCHAR(75))  BEGIN
	SELECT 
		cve_usuario,
		CONCAT(nombre_usuario, ' ', apellidop_usuario, ' ', apellidom_usuario) AS nombreCompleto,
		cveperfil_usuario,
		login_usuario, 
		cvesucursal_usuario,
		nombre_sucursal,
		(SELECT COUNT(*) FROM ca_usuario WHERE login_usuario = usuario AND password_usuario = pass AND estatus_usuario = 1) AS total_rows 
	FROM ca_usuario
	left join ca_sucursales on cve_sucursal = cvesucursal_usuario
	WHERE login_usuario = usuario 
	AND password_usuario = pass 
	AND estatus_usuario = 1;

END$$

DROP PROCEDURE IF EXISTS `obtenClientes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenClientes` (IN `ban` INT, IN `cveCliente` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los Clientes activos
	-- 2. Obtenemos un cliente en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				
				if cve_sucursal_i > 0 then 
					SELECT 
						*
					FROM ca_clientes where 1=1 and cvesucursal_cliente = cve_sucursal_i order by nombre_cliente asc;
				else 
					SELECT 
						*
					FROM ca_clientes where 1 order by nombre_cliente asc;
				end if;
				
		WHEN ban = 2 THEN
			
				if cve_sucursal_i > 0 then 
					SELECT 
						*
					FROM ca_clientes 
					WHERE cve_cliente = cveCliente and cvesucursal_cliente = cve_sucursal_i order by nombre_cliente asc;
				else 
					SELECT 
						*
					FROM ca_clientes 
					WHERE cve_cliente = cveCliente order by nombre_cliente asc;
				end if;
				
		END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenerFolio`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenerFolio` (IN `ban` INT, IN `cve_venta_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los Usuarios activos
	-- 2. Obtenemos un usuario en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					*
				FROM ma_ventas AS mv 
				left join ca_clientes cc on cc.cve_cliente = mv.cvecliente_venta 
				WHERE mv.cve_venta = cve_venta_i;
				
				
		END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenListaventa`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenListaventa` (IN `ban` INT, IN `cve_venta_i` INT, IN `tipo_venta_i` INT, IN `estatus_venta_i` INT, IN `fecha1_venta_i` VARCHAR(100), IN `fecha2_venta_i` VARCHAR(100), IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos losPRODUCTOS
	-- 2. Obtenemos un PRODUCTO en especifico


	CASE
		WHEN ban = 1 THEN
				
				if cve_sucursal_i > 0 then 
					select 
					mv.*,
					cu.nombre_usuario,
					cu.apellidop_usuario,
					date_format(mv.fechaadd_venta, "%Y-%m-%d") as fechaventa
					from ma_ventas mv
					left join ca_usuario cu on cu.cve_usuario = mv.cveusuarioadd_venta 
					where mv.cvesucursal_venta = cve_sucursal_i and mv.estatus_venta = estatus_venta_i and date_format(mv.fechaadd_venta, "%Y-%m-%d") between fecha1_venta_i and fecha2_venta_i order by mv.folio_venta desc;
				else 
					select 
					mv.*,
					cu.nombre_usuario,
					cu.apellidop_usuario,
					date_format(mv.fechaadd_venta, "%Y-%m-%d %T") as fechaventa	
					from ma_ventas mv
					left join ca_usuario cu on cu.cve_usuario = mv.cveusuarioadd_venta 
					where 1 = 1 and mv.estatus_venta = estatus_venta_i and mv.tipo_venta = tipo_venta_i
					and date_format(mv.fechaadd_venta, "%Y-%m-%d") between fecha1_venta_i and fecha2_venta_i order by mv.folio_venta desc;
				end if;
				
		WHEN ban = 2 then
			
			if tipo_venta_i = 1 then
		
				select * from ma_ventas mv 
					left join ca_usuario cu on cu.cve_usuario = mv.cveusuarioadd_venta
					inner join de_venta dv on dv.cveventa_venta = mv.cve_venta 
					inner join ca_productos cp on cp.cve_producto = dv.cvesp_venta 
					where mv.cve_venta = cve_venta_i order by mv.folio_venta asc;
			
			else	
			
				select * from ma_ventas mv 
					left join ca_usuario cu on cu.cve_usuario = mv.cveusuarioadd_venta
					inner join de_venta dv on dv.cveventa_venta = mv.cve_venta 
					inner join ca_sabores cs on cs.cve_sabor = dv.cvesp_venta 
					inner join ca_productos cp on cp.cve_producto = cs.cveproducto_sabor 
					where mv.cve_venta = cve_venta_i order by mv.folio_venta asc;
				
			end if;
									
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenPerfiles`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenPerfiles` (IN `ban` INT, IN `cv_perfil_i` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los PERFILES
	-- 2. Obtenemos un perfil en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				
			if cve_sucursal_i > 0 then 
				SELECT cve_perfil,nombre_perfil,descripcion_perfil,estatus_perfil FROM ca_perfil WHERE 1=1 and cvesucursal_perfil = cve_sucursal_i order by nombre_perfil asc;
			else 
				SELECT cve_perfil,nombre_perfil,descripcion_perfil,estatus_perfil FROM ca_perfil WHERE 1 order by nombre_perfil asc;
			end if;
				
		WHEN ban = 2 THEN
		
			if cve_sucursal_i > 0 then 
				SELECT cve_perfil,nombre_perfil,descripcion_perfil,estatus_perfil FROM ca_perfil WHERE cve_perfil = cv_perfil_i and cvesucursal_perfil = cve_sucursal_i order by nombre_perfil asc;
			else 
				SELECT cve_perfil,nombre_perfil,descripcion_perfil,estatus_perfil FROM ca_perfil WHERE cve_perfil = cv_perfil_i order by nombre_perfil asc;
			end if;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenProducto`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenProducto` (IN `ban` INT, IN `cve_producto_i` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos losPRODUCTOS
	-- 2. Obtenemos un PRODUCTO en especifico


	CASE
		WHEN ban = 1 THEN
				
				if cve_sucursal_i > 0 then 
					SELECT 
						*
					FROM ca_productos WHERE 1 = 1 and cvesucursal_producto = cve_sucursal_i order by nombre_producto asc;
				else 
					SELECT 
						*
					FROM ca_productos WHERE 1 = 1 order by nombre_producto asc;
				end if;
				
		WHEN ban = 2 then
		
				if cve_sucursal_i > 0 then 
					SELECT * FROM ca_productos WHERE cve_producto = cve_producto_i and cvesucursal_producto = cve_sucursal_i order by nombre_producto asc;
				else 
					SELECT * FROM ca_productos WHERE cve_producto = cve_producto_i order by nombre_producto asc;
				end if;
			
		WHEN ban = 3 then
		
				if cve_sucursal_i > 0 then 
					SELECT 
					*
					FROM ca_productos WHERE estatus_producto = 1 and cvesucursal_producto = cve_sucursal_i order by nombre_producto asc;
				else 
					SELECT 
					*
					FROM ca_productos WHERE estatus_producto = 1 order by nombre_producto asc;
				end if;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenPuestos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenPuestos` (IN `ban` INT, IN `cve_puesto_i` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los PUESTOS
	-- 2. Obtenemos un PUESTO en especifico
	
	CASE
		WHEN ban = 1 THEN
		
			if cve_sucursal_i > 0 then 
				SELECT 
					cve_puesto,
					nombre_puesto,
					estatus_puesto
				FROM ca_puestos WHERE 1 and cvesucursal_puesto = cve_sucursal_i ORDER BY nombre_puesto asc;
			else 
				SELECT 
					cve_puesto,
					nombre_puesto,
					estatus_puesto
				FROM ca_puestos WHERE 1 ORDER BY nombre_puesto asc;
			end if;
				
		WHEN ban = 2 THEN
		
			if cve_sucursal_i > 0 then 
				SELECT cve_puesto,nombre_puesto,estatus_puesto FROM ca_puestos WHERE cve_puesto = cve_puesto_i  and cvesucursal_puesto = cve_sucursal_i ORDER BY nombre_puesto asc;
			else 
				SELECT cve_puesto,nombre_puesto,estatus_puesto FROM ca_puestos WHERE cve_puesto = cve_puesto_i ORDER BY nombre_puesto asc;
			end if;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenSabores`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenSabores` (IN `ban` INT, IN `cve_sabor_i` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los SABORES
	-- 2. Obtenemos un SABOR en especifico
	
	CASE
		WHEN ban = 1 THEN
	
			if cve_sucursal_i > 0 then 
				SELECT 
					cs.*, cp.cve_producto, cp.nombre_producto 
				FROM ca_sabores cs
				inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor and cp.cvesucursal_producto = cve_sucursal_i
				WHERE 1 order by cp.nombre_producto, cs.nombre_sabor asc;
			else 
				SELECT 
					cs.*, cp.cve_producto, cp.nombre_producto 
				FROM ca_sabores cs
				inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor
				WHERE 1 order by cp.nombre_producto, cs.nombre_sabor asc;
			end if;
				
		WHEN ban = 2 then
		
				if cve_sucursal_i > 0 then 
					SELECT 
						cs.*, cp.cve_producto, cp.nombre_producto 
					FROM ca_sabores cs
					inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor and cp.cvesucursal_producto = cve_sucursal_i
					where cve_sabor = cve_sabor_i order by cp.nombre_producto, cs.nombre_sabor asc;
				else 
					SELECT 
						cs.*, cp.cve_producto, cp.nombre_producto 
					FROM ca_sabores cs
					inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor
					where cve_sabor = cve_sabor_i order by cp.nombre_producto, cs.nombre_sabor asc;
				end if;
	
		WHEN ban = 3 THEN
		
				if cve_sucursal_i > 0 then 
					SELECT 
						cs.*, cp.* 
					FROM ca_sabores cs
					inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor and cp.cvesucursal_producto = cve_sucursal_i
					WHERE cs.estatus_sabor = 1 and cveproducto_sabor = cve_sabor_i order by cp.nombre_producto, cs.nombre_sabor asc;
				else 
					SELECT 
						cs.*, cp.* 
					FROM ca_sabores cs
					inner join ca_productos cp on cp. cve_producto =   cs.cveproducto_sabor 
					WHERE cs.estatus_sabor = 1 and cveproducto_sabor = cve_sabor_i order by cp.nombre_producto, cs.nombre_sabor asc;
				end if;
				
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenSucursales`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenSucursales` (IN `ban` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos las SUCURSALE
	-- 2. Obtenemos una SUCURSAL en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					cve_sucursal,
					nombre_sucursal,
					CONCAT(calle_sucursal," ",colonia_sucursal) AS direccion_sucursal,
					telefono_sucursal,
					representante_sucursal,
					IF(tipo_sucursal = 1,"SUCURSAL","ALMACÃ‰N") AS tipo_sucursal,
					estatus_sucursal 
				FROM ca_sucursales WHERE 1  order by nombre_sucursal asc;
				
		WHEN ban = 2 THEN
		
				SELECT cve_sucursal,nombre_sucursal,calle_sucursal,colonia_sucursal,telefono_sucursal,representante_sucursal,tipo_sucursal FROM ca_sucursales WHERE cve_sucursal = cve_sucursal_i  order by nombre_sucursal asc;
				
	END CASE;

END$$

DROP PROCEDURE IF EXISTS `obtenUsuarios`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtenUsuarios` (IN `ban` INT, IN `cveUsuario` INT, IN `cve_sucursal_i` INT)  BEGIN
	
	-- 1. Obtenemos todos los Usuarios activos
	-- 2. Obtenemos un usuario en especifico
	
	CASE
		WHEN ban = 1 THEN
		
				
				
				if cve_sucursal_i > 0 then 
					SELECT 
						a1.cve_usuario,
						CONCAT(a1.nombre_usuario,' ',a1.apellidop_usuario,' ',a1.apellidom_usuario) AS nombreCompleto,
						a1.login_usuario,
						b1.nombre_perfil,
						c1.nombre_sucursal,
						d1.nombre_puesto,
						a1.estatus_usuario
					FROM ca_usuario AS a1 
					LEFT JOIN ca_perfil AS b1 ON b1.cve_perfil = a1.cveperfil_usuario
					LEFT JOIN ca_sucursales AS c1 ON c1.cve_sucursal = a1.cvesucursal_usuario
					LEFT JOIN ca_puestos AS d1 ON d1.cve_puesto = a1.cvepuesto_usuario
					WHERE 1 and a1.cvesucursal_usuario = cve_sucursal_i ORDER BY a1.nombre_usuario asc;
				else 
					SELECT 
						a1.cve_usuario,
						CONCAT(a1.nombre_usuario,' ',a1.apellidop_usuario,' ',a1.apellidom_usuario) AS nombreCompleto,
						a1.login_usuario,
						b1.nombre_perfil,
						c1.nombre_sucursal,
						d1.nombre_puesto,
						a1.estatus_usuario
					FROM ca_usuario AS a1 
					LEFT JOIN ca_perfil AS b1 ON b1.cve_perfil = a1.cveperfil_usuario
					LEFT JOIN ca_sucursales AS c1 ON c1.cve_sucursal = a1.cvesucursal_usuario
					LEFT JOIN ca_puestos AS d1 ON d1.cve_puesto = a1.cvepuesto_usuario
					WHERE 1 ORDER BY a1.nombre_usuario asc;
				end if;
		WHEN ban = 2 THEN
		
				
			
			if cve_sucursal_i > 0 then 
				SELECT 
					a1.cve_usuario,
					a1.nombre_usuario,
					a1.apellidop_usuario,
					a1.apellidom_usuario,
					a1.login_usuario,
					a1.cveperfil_usuario,
					a1.cvesucursal_usuario,
					a1.cvepuesto_usuario,
					b1.nombre_perfil,
					c1.nombre_sucursal,
					d1.nombre_puesto
				FROM ca_usuario AS a1 
				LEFT JOIN ca_perfil AS b1 ON b1.cve_perfil = a1.cveperfil_usuario
				LEFT JOIN ca_sucursales AS c1 ON c1.cve_sucursal = a1.cvesucursal_usuario
				LEFT JOIN ca_puestos AS d1 ON d1.cve_puesto = a1.cvepuesto_usuario
				WHERE a1.cve_usuario = cveUsuario and a1.cvesucursal_usuario = cve_sucursal_i ORDER BY a1.nombre_usuario asc;
			else 
				SELECT 
					a1.cve_usuario,
					a1.nombre_usuario,
					a1.apellidop_usuario,
					a1.apellidom_usuario,
					a1.login_usuario,
					a1.cveperfil_usuario,
					a1.cvesucursal_usuario,
					a1.cvepuesto_usuario,
					b1.nombre_perfil,
					c1.nombre_sucursal,
					d1.nombre_puesto
				FROM ca_usuario AS a1 
				LEFT JOIN ca_perfil AS b1 ON b1.cve_perfil = a1.cveperfil_usuario
				LEFT JOIN ca_sucursales AS c1 ON c1.cve_sucursal = a1.cvesucursal_usuario
				LEFT JOIN ca_puestos AS d1 ON d1.cve_puesto = a1.cvepuesto_usuario
				WHERE a1.cve_usuario = cveUsuario ORDER BY a1.nombre_usuario asc;
			end if;
				
		END CASE;

END$$

DROP PROCEDURE IF EXISTS `obten_opcionesperfil`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obten_opcionesperfil` (IN `ban` INT, IN `cvePerfil_i` INT, IN `opcion_i` INT)  BEGIN
	
	-- 1. Obtenemos todos las opciones R ordenados dependiendo el perfil del usuario
	-- 2. Obtenemos todos las opciones M ordenados dependiendo la opcion R y el perfil del usuario
	-- 3. Obtenemos todos las opciones R ordenados
	-- 4. Obtenemos todos las opciones M ordenados dependiendo la opcion R
	-- 5. Obtenemos todos las opciones M ordenados dependiendo la opcion R y el perfil del usuario comparando con los ya guardados
	-- 6. Obtenemos todas las opciones dependiendo el perfil
	
	CASE
		WHEN ban = 1 THEN
		
				SELECT 
					cve_opcion,
					nombre_opcion,
					icono,
					render_opcion,
					orden_opcion
				FROM ca_opcion
				WHERE cve_opcion 
				IN (SELECT b.cveopcion_opcion
				FROM de_perfilopcion AS a
				INNER JOIN ca_opcion AS b ON b.cve_opcion = a.cveopcion_perfilopcion
				WHERE a.cveperfil_perfilopcion = cvePerfil_i GROUP BY b.cveopcion_opcion)
				
				ORDER BY orden_opcion ASC;
				
		WHEN ban = 2 THEN
		
				SELECT 
					cao.cve_opcion,
					cao.nombre_opcion,
					cao.metodo_opcion,
					cao.render_opcion,
					cao.orden_opcion
				FROM ca_opcion AS cao
				INNER JOIN de_perfilopcion AS dep ON dep.cveopcion_perfilopcion = cao.cve_opcion
				WHERE dep.cveperfil_perfilopcion = cvePerfil_i AND cao.cveopcion_opcion = opcion_i;
				
		WHEN ban = 3 THEN
		
				SELECT 
					cve_opcion,
					nombre_opcion,
					icono,
					render_opcion,
					orden_opcion
				FROM ca_opcion
				WHERE render_opcion = 'R'
				
				ORDER BY orden_opcion ASC;
				
		WHEN ban = 4 THEN
		
				SELECT 
					cao.cve_opcion,
					cao.nombre_opcion,
					cao.metodo_opcion,
					cao.render_opcion,
					cao.orden_opcion
				FROM ca_opcion AS cao
				-- INNER JOIN de_perfilopcion AS dep ON dep.cveopcion_perfilopcion = cao.cve_opcion
				WHERE cao.cveopcion_opcion = opcion_i;
				
		WHEN ban = 5 THEN
		
				SELECT 
					cao.cve_opcion,
					cao.nombre_opcion,
					cao.metodo_opcion,
					cao.render_opcion,
					cao.orden_opcion,
					IF((SELECT COUNT(*) AS existe FROM de_perfilopcion WHERE cveperfil_perfilopcion = cvePerfil_i AND cveopcion_perfilopcion = cao.cve_opcion) > 0,1,0) AS chk
				FROM ca_opcion AS cao
				-- INNER JOIN de_perfilopcion AS dep ON dep.cveopcion_perfilopcion = cao.cve_opcion
				WHERE cao.cveopcion_opcion = opcion_i;
				
		WHEN ban = 6 THEN
		
				SELECT * FROM de_perfilopcion WHERE cveperfil_perfilopcion = cvePerfil_i;
				
		END CASE;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_opcion`
--

DROP TABLE IF EXISTS `ca_opcion`;
CREATE TABLE `ca_opcion` (
  `cve_opcion` int(11) NOT NULL,
  `nombre_opcion` varchar(120) DEFAULT NULL,
  `descripcion_opcion` varchar(255) DEFAULT NULL,
  `cveopcion_opcion` int(5) DEFAULT 0,
  `archivo_opcion` varchar(120) DEFAULT NULL,
  `metodo_opcion` varchar(120) DEFAULT NULL,
  `icono` varchar(120) DEFAULT NULL COMMENT 'ICONOS DE LA MISMA LIBRERIA',
  `orden_opcion` int(5) DEFAULT 0,
  `render_opcion` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_opcion`
--

INSERT INTO `ca_opcion` (`cve_opcion`, `nombre_opcion`, `descripcion_opcion`, `cveopcion_opcion`, `archivo_opcion`, `metodo_opcion`, `icono`, `orden_opcion`, `render_opcion`) VALUES
(1, 'SISTEMA', 'SECCIÓN PARA LA ADMINISTRACIÓN DE CATÁLOGOS, PERFILES Y USUARIOS', 0, NULL, NULL, 'fa fa-cogs', 10, 'R'),
(2, 'USUARIO', 'MÓDULO PARA AGREGAR USUARIOS Y ASIGNAR PERFILES', 1, 'Usuario.php', 'usuario', NULL, 1, 'M'),
(3, 'PERFIL', 'MÓDULO PARA GENERAR PERFILES Y DAR ACCESOS', 1, 'Perfil.php', 'perfil', NULL, 2, 'M'),
(4, 'SUCURSALES', 'MÓDULO PARA AGREGAR SUCURSALES', 1, 'Sucursal.php', 'sucursal', NULL, 3, 'M'),
(5, 'PUESTOS', 'MÓDULO', 1, 'Puesto.php', 'puesto', NULL, 4, 'M'),
(6, 'CATÁLOGOS', 'SECCIÓN PARA LA ADMINISTRACIÓN DE PRODUCTOS, SABORES Y CLIENTES', 0, NULL, NULL, 'fa fa-list', 9, 'R'),
(7, 'PRODUCTOS', 'MÓDULO PARA AGREGAR PRODUCTOS', 6, 'Producto.php', 'producto', NULL, 1, 'M'),
(8, 'SABORES', 'MÓDULO PARA AGREGAR SABORES', 6, 'Sabor.php', 'sabor', NULL, 2, 'M'),
(9, 'CLIENTES', 'MÓDULO PARA AGREGAR CLIENTES', 6, 'Cliente.php', 'cliente', NULL, 3, 'M'),
(10, 'VENTAS', 'SECCIÓN PARA LA ADMINISTRACIÓN DE PRODUCTOS, SABORES Y CLIENTES', 0, NULL, NULL, 'fa fa-shopping-cart', 8, 'R'),
(11, 'LISTA VENTAS', 'MÓDULO PARA AGREGAR PRODUCTOS', 10, 'Listaventa.php', 'listaventa', NULL, 1, 'M'),
(12, 'CANCELACION ', 'MÓDULO PARA AGREGAR SABORES', 10, 'Cancelacion.php', 'cancelacion', NULL, 2, 'M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_perfil`
--

DROP TABLE IF EXISTS `ca_perfil`;
CREATE TABLE `ca_perfil` (
  `cve_perfil` int(11) NOT NULL,
  `nombre_perfil` varchar(120) DEFAULT NULL,
  `descripcion_perfil` varchar(255) DEFAULT NULL,
  `estatus_perfil` int(1) DEFAULT 0,
  `cvesucursal_perfil` int(11) DEFAULT NULL,
  `cveusuarioalta_perfil` int(5) DEFAULT 0,
  `fechaalta_perfil` datetime DEFAULT NULL,
  `cveusuariomod_perfil` int(5) DEFAULT NULL,
  `fechamod_perfil` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_perfil`
--

INSERT INTO `ca_perfil` (`cve_perfil`, `nombre_perfil`, `descripcion_perfil`, `estatus_perfil`, `cvesucursal_perfil`, `cveusuarioalta_perfil`, `fechaalta_perfil`, `cveusuariomod_perfil`, `fechamod_perfil`) VALUES
(1, 'SYSADMIN', 'ACCESO A TODOS LOS MÓDULOS DEL SISTEMA', 1, NULL, 1, '2019-07-21 13:24:19', 1, '2021-08-15 18:26:01'),
(2, 'ADMINISTRADOR', 'ACCESO A TODOS LOS MODULOS', 1, 0, 1, '2021-08-12 00:42:41', 1, '2021-08-15 18:22:40'),
(3, 'VENDEDOR', 'ACCESO A VENTAS', 1, 0, 1, '2021-08-12 01:14:14', 1, '2021-08-12 01:20:38'),
(4, 'CAJERO', 'ACESO SOLO A LISTA DE VENTA', 1, 0, 1, '2021-08-18 02:51:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_puestos`
--

DROP TABLE IF EXISTS `ca_puestos`;
CREATE TABLE `ca_puestos` (
  `cve_puesto` int(255) NOT NULL,
  `nombre_puesto` varchar(120) DEFAULT NULL,
  `estatus_puesto` int(1) DEFAULT NULL,
  `cvesucursal_puesto` int(11) DEFAULT NULL,
  `cveusuarioalta_puesto` int(5) DEFAULT NULL,
  `fechaalta_puesto` datetime DEFAULT NULL,
  `cveusuariomod_puesto` int(5) DEFAULT NULL,
  `fechamod_puesto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_puestos`
--

INSERT INTO `ca_puestos` (`cve_puesto`, `nombre_puesto`, `estatus_puesto`, `cvesucursal_puesto`, `cveusuarioalta_puesto`, `fechaalta_puesto`, `cveusuariomod_puesto`, `fechamod_puesto`) VALUES
(1, 'ADMINISTRADOR', 1, 0, 1, '2021-08-12 00:38:23', NULL, NULL),
(2, 'VENDEDOR', 1, 0, 1, '2021-08-12 00:50:02', NULL, NULL),
(3, 'CAJERO', 1, 0, 1, '2021-08-18 02:55:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_sucursales`
--

DROP TABLE IF EXISTS `ca_sucursales`;
CREATE TABLE `ca_sucursales` (
  `cve_sucursal` int(11) NOT NULL,
  `tipo_sucursal` int(2) DEFAULT NULL COMMENT '1-Sucursal, 2-AlmacÃ©n',
  `nombre_sucursal` varchar(150) DEFAULT NULL,
  `calle_sucursal` varchar(255) DEFAULT NULL,
  `colonia_sucursal` varchar(75) DEFAULT NULL,
  `telefono_sucursal` varchar(75) DEFAULT NULL,
  `representante_sucursal` varchar(120) DEFAULT NULL,
  `estatus_sucursal` int(1) DEFAULT NULL,
  `cveusuarioalta_sucursal` int(5) DEFAULT NULL,
  `fechaalta_sucursal` datetime DEFAULT NULL,
  `cveusuariomod_sucursal` int(5) DEFAULT NULL,
  `fechamod_sucursal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_sucursales`
--

INSERT INTO `ca_sucursales` (`cve_sucursal`, `tipo_sucursal`, `nombre_sucursal`, `calle_sucursal`, `colonia_sucursal`, `telefono_sucursal`, `representante_sucursal`, `estatus_sucursal`, `cveusuarioalta_sucursal`, `fechaalta_sucursal`, `cveusuariomod_sucursal`, `fechamod_sucursal`) VALUES
(1, 1, 'HELADOS ALTEÑA PRINCIPAL', 'AV. LIBRAMIENTO 2, ESQUINA CON AV. MANUEL J. CLOUTHIER #100', 'FRACC, LOS CONCHIS', '6692158585', 'DIR. MIGUEL ANGEL MEDRANO ROMERO', 1, 1, '2019-08-02 23:03:21', 1, '2021-07-31 12:58:20'),
(2, 1, 'HELADOS DOÑA MARY', 'CERRADA CLAVELES 2', '317', '6691998781', 'LOS TRES HERMANOS', 1, 1, '2021-08-12 00:40:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ca_usuario`
--

DROP TABLE IF EXISTS `ca_usuario`;
CREATE TABLE `ca_usuario` (
  `cve_usuario` int(11) NOT NULL,
  `cveperfil_usuario` int(5) DEFAULT 0,
  `cvesucursal_usuario` int(5) DEFAULT NULL,
  `cvepuesto_usuario` int(5) DEFAULT NULL,
  `login_usuario` varchar(120) DEFAULT NULL,
  `password_usuario` varchar(120) DEFAULT NULL,
  `nombre_usuario` varchar(75) DEFAULT NULL,
  `apellidop_usuario` varchar(75) DEFAULT NULL,
  `apellidom_usuario` varchar(75) DEFAULT NULL,
  `estatus_usuario` int(1) DEFAULT 0,
  `cveusuarioalta_usuario` int(11) DEFAULT NULL,
  `fechaalta_usuario` datetime DEFAULT NULL,
  `cveusuariomod_usuario` int(11) DEFAULT NULL,
  `fechamod_usuario` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `ca_usuario`
--

INSERT INTO `ca_usuario` (`cve_usuario`, `cveperfil_usuario`, `cvesucursal_usuario`, `cvepuesto_usuario`, `login_usuario`, `password_usuario`, `nombre_usuario`, `apellidop_usuario`, `apellidom_usuario`, `estatus_usuario`, `cveusuarioalta_usuario`, `fechaalta_usuario`, `cveusuariomod_usuario`, `fechamod_usuario`) VALUES
(1, 1, 0, 1, 'jorge', 'b3743d2588b110cc6f2ba43f1758a757', 'JORGE', 'VALENZUELA', 'SANTIAGO', 1, 1, '2019-07-27 00:45:44', 1, '2019-08-06 13:52:21'),
(2, 2, 1, 1, 'luis', 'b3743d2588b110cc6f2ba43f1758a757', 'LUIS', 'GARCIA', 'MONTES', 1, 1, '2021-08-12 01:08:31', 1, '2021-08-12 01:19:49'),
(3, 3, 1, 2, 'alfredo', 'b3743d2588b110cc6f2ba43f1758a757', 'ALFREDO', 'RODRIGUEZ', 'LUNA', 1, 1, '2021-08-12 01:08:37', 1, '2021-08-12 01:21:03'),
(4, 2, 2, 1, 'lorena', 'b3743d2588b110cc6f2ba43f1758a757', 'LORENA', 'MONTES', 'VARGAS', 1, 1, '2021-08-12 01:32:35', NULL, NULL),
(5, 3, 2, 2, 'mario', 'b3743d2588b110cc6f2ba43f1758a757', 'MARIO', 'GARCIA', 'BAUTISTA', 1, 1, '2021-08-12 01:33:22', NULL, NULL),
(6, 4, 2, 2, 'cajeromary', 'b3743d2588b110cc6f2ba43f1758a757', 'CAJERO', 'MATCAJERO', 'APECAJERO', 1, 1, '2021-08-18 02:52:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `de_perfilopcion`
--

DROP TABLE IF EXISTS `de_perfilopcion`;
CREATE TABLE `de_perfilopcion` (
  `cve_perfilopcion` int(11) NOT NULL,
  `cveperfil_perfilopcion` int(11) DEFAULT NULL,
  `cveopcion_perfilopcion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `de_perfilopcion`
--

INSERT INTO `de_perfilopcion` (`cve_perfilopcion`, `cveperfil_perfilopcion`, `cveopcion_perfilopcion`) VALUES
(377, 2, 7),
(378, 2, 8),
(379, 2, 9),
(380, 2, 11),
(381, 2, 12),
(382, 1, 2),
(383, 1, 3),
(384, 1, 4),
(385, 1, 5),
(386, 1, 7),
(387, 1, 8),
(388, 1, 9),
(389, 1, 11),
(390, 1, 12),
(391, 4, 11);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ca_opcion`
--
ALTER TABLE `ca_opcion`
  ADD PRIMARY KEY (`cve_opcion`) USING BTREE;

--
-- Indices de la tabla `ca_perfil`
--
ALTER TABLE `ca_perfil`
  ADD PRIMARY KEY (`cve_perfil`) USING BTREE;

--
-- Indices de la tabla `ca_puestos`
--
ALTER TABLE `ca_puestos`
  ADD PRIMARY KEY (`cve_puesto`) USING BTREE;

--
-- Indices de la tabla `ca_sucursales`
--
ALTER TABLE `ca_sucursales`
  ADD PRIMARY KEY (`cve_sucursal`) USING BTREE;

--
-- Indices de la tabla `ca_usuario`
--
ALTER TABLE `ca_usuario`
  ADD PRIMARY KEY (`cve_usuario`) USING BTREE;

--
-- Indices de la tabla `de_perfilopcion`
--
ALTER TABLE `de_perfilopcion`
  ADD PRIMARY KEY (`cve_perfilopcion`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ca_opcion`
--
ALTER TABLE `ca_opcion`
  MODIFY `cve_opcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `ca_perfil`
--
ALTER TABLE `ca_perfil`
  MODIFY `cve_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ca_puestos`
--
ALTER TABLE `ca_puestos`
  MODIFY `cve_puesto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ca_sucursales`
--
ALTER TABLE `ca_sucursales`
  MODIFY `cve_sucursal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ca_usuario`
--
ALTER TABLE `ca_usuario`
  MODIFY `cve_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `de_perfilopcion`
--
ALTER TABLE `de_perfilopcion`
  MODIFY `cve_perfilopcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
